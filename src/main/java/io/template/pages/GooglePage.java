package io.template.pages;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Google")
public class GooglePage extends AnyPage{

    @FindBy(css = "input#lst-ib")
    @ElementTitle("search")
    public WebElement inputSearch;

    @FindBy(css = "input[name='btnI']")
    @ElementTitle("feeling lucky")
    public WebElement buttonFeelingLucky;

    public GooglePage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), 60).until(ExpectedConditions.visibilityOf(inputSearch));
    }
}
