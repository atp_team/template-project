package io.template.pages.linkedIn;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Login")
public class LoginPage extends AnyPage {

    @FindBy(id = "session_key-login")
    @ElementTitle("login")
    public WebElement inputLogin;

    @FindBy(id = "session_password-login")
    @ElementTitle("password")
    public WebElement inputPassword;

    @FindBy(css = "input[name='signin']")
    @ElementTitle("button login")
    public WebElement buttonLogin;

    public LoginPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.elementToBeClickable(inputLogin));
    }

    public void login(String login, String password) throws Exception {
        fill_field(inputLogin, login);
        fill_field(inputPassword, password);
        press_button_by_js(buttonLogin);
    }

}
