package io.template.pages.linkedIn;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Search")
public class SearchPage extends AnyPage {

    @FindBy(css = "div.search-results-page")
    @ElementTitle("login")
    public WebElement initElem;

    public SearchPage() {
        Init.getDriver().get("https://www.linkedin.com/search/results/people/?facetGeoRegion=%5B%22us%3A84%22%2C%22us%3A0%22%5D&keywords=HR%20Managers&origin=FACETED_SEARCH&page=1");
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(initElem));
    }

    public void make_search() throws Exception {
        Init.getDriverExtensions().waitForPageToLoad();
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("ul.results-list  li span.actor-name")));
        for (int i = 0; i < 5; i++) {
            new Actions(Init.getDriver()).sendKeys(Keys.END).build().perform();
            sleep(1);
        }
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("ul.results-list  li span.actor-name")));
        List<WebElement> result = Init.getDriver().findElements(By.cssSelector("ul.results-list > li"));
        result.forEach(res -> {
            System.out.println("id: " + res.getAttribute("id"));
            System.out.println("name: " + res.findElement(By.xpath(".//span[contains(@class, 'actor-name')]")).getText());
        });
    }

}
