package io.template.pages.yelp;

import io.template.lib.Init;
import io.template.lib.pageFactory.AutotestError;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Yelp. Search")
public class YelpSearchPage extends AnyPage {

    private static String CATEGORY_XPATH = "//li[@data-suggestion-type='category']//strong[contains(text(), '%s')]";
    private static String RESULT_CSS = "ul.search-results li[class*='search-result']";

    @FindBy(id = "find_desc")
    @ElementTitle("search input")
    public WebElement inputSearch;

    @FindBy(css = "span.pagination-results-window")
    @ElementTitle("result count")
    public WebElement spanResultCount;

    @FindBy(css = "button.main-search_submit")
    @ElementTitle("Search")
    public WebElement buttonSearch;

    @FindBy(css = "div.filter-panel_filters input[value='RestaurantsPriceRange2.3']")
    @ElementTitle("price $$$")
    public WebElement inputPrice3;

    @FindBy(css = "ul.main input[value*='Union_Square']")
    @ElementTitle("filter Union Square")
    public WebElement inputUnionSquare;

    @FindBy(css = "ul.main input[value*='Embarcadero']")
    @ElementTitle("filter Embarcadero")
    public WebElement inputEmbarcadero;

    @FindBy(css = "ul.search-results li[class*='search-result']")
    @ElementTitle("search result")
    public List<WebElement> ulSearchResult;

    @FindBy(css = "span.all-filters-toggle")
    @ElementTitle("Show all filters toogle")
    public WebElement spanShowAllFilters;


    public YelpSearchPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(inputSearch));
    }

    public void select_category() throws IllegalAccessException, InterruptedException {
        press_button(inputSearch);
        toAllure("Will used", "Category: " + Init.getStash().get("category"));
        WebElement category = Init.getDriver().findElement(By.xpath(String.format(CATEGORY_XPATH, Init.getStash().get("category"))));
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(category));
        press_button(category);
        Init.getDriverExtensions().waitForPageToLoad();
        press_button(buttonSearch);
        Init.getDriverExtensions().waitForPageToLoad();
        sleep(2);
    }

    public void append_to_search_string() throws InterruptedException, IllegalAccessException {
        System.out.println("our cate: " + Init.getStash().get("appendText"));
        toAllure("Will used", "appendText: " + Init.getStash().get("appendText"));
        inputSearch.sendKeys(" " + Init.getStash().get("appendText"));
        sleep(2);
        Init.getDriverExtensions().waitForPageToLoad();
        press_button(buttonSearch);
        Init.getDriverExtensions().waitForPageToLoad();
        sleep(2);
    }

    public void report_page_count() {
        String totalCount = spanResultCount.getText().split("of")[1];
        String inCurrentPage = spanResultCount.getText().split("of")[0].split("-")[1];
        System.out.println("Search result" + "Total count: " + totalCount + ", in current page: " + inCurrentPage);
        toAllure("Search result", "Total count: " + totalCount + ", in current page: " + inCurrentPage);
    }

    public void set_filter() throws IllegalAccessException, InterruptedException {
        Init.getDriverExtensions().waitForPageToLoad();
        press_button(spanShowAllFilters);
        sleep(2);
        Init.getDriverExtensions().waitForPageToLoad();
        String[] filters = Init.getStash().get("filters").toString().split(";");
        for (String filter : filters) {
            switch (filter) {
                case "$$$":
                    press_button(inputPrice3);
                    break;
                case "Union Square":
                    press_button(inputUnionSquare);
                    break;
                case "Embarcadero":
                    press_button(inputEmbarcadero);
                    break;
                default:
                    throw new AutotestError("Unknown case: " + filter);
            }
            sleep(5);
            Init.getDriverExtensions().waitForPageToLoad();
        }
    }


    public void report_star_rating() {
        Init.getDriverExtensions().waitForPageToLoad();
        ulSearchResult.forEach(el -> {
            String stars = el.findElement(By.xpath(".//div[contains(@class, 'rating-large')]/img")).getAttribute("alt");
            String name = el.findElement(By.xpath(".//a[contains(@class, 'biz-name')]//span")).getText();
            System.out.println("Name: " + name + ", stars: " + stars);
            toAllure("Name: " + name, "Stars: " + stars);
        });
    }

    public void open_result(String number) throws IllegalAccessException, InterruptedException {
        Init.getDriver().get(ulSearchResult.get(Integer.parseInt(number) - 1).findElement(
                By.xpath(".//a[contains(@class, 'biz-name')]")).getAttribute("href"));
        Init.getDriverExtensions().waitForPageToLoad();
    }
}
