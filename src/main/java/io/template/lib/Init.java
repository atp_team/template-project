package io.template.lib;

import io.template.lib.db.DatabaseConnectionFactory;
import io.template.lib.pageFactory.AutotestError;
import io.template.lib.pageFactory.PageFactory;
import io.template.lib.util.OsCheck;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Attachment;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {

    private static final Map<String, Object> stash = new HashMap<>();
    private static WebDriver driver;
    private static PageFactory pageFactory;
    private static String os = Props.get("automation.os");
    private static String testInstrument = Props.get("automation.instrument");
    private static String timeout = Props.get("webdriver.page.load.timeout");
    private static DatabaseConnectionFactory dbConnectionFactory;

    public static WebDriver getDriver() {
        if (null == driver) {
            if (os.equalsIgnoreCase("Android") ||
                    os.equalsIgnoreCase("IOS"))
                return getBrowserStackDriver();

            try {
                createWebDriver();
            } catch (UnreachableBrowserException e) {
                System.err.println("Failed to create web driver" + e.getMessage());
                int i = 2;
                while (i > 0) {
                    dispose();
                    try {
                        createWebDriver();
                    } catch (UnreachableBrowserException e1) {
                        System.err.println("Failed to create web driver. Try count = " + i + ". Error message" + e1.getMessage());
                    }
                    i--;
                }
            }
        }
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        Init.driver = driver;
    }

    public static PageFactory getPageFactory() {
        if (null == pageFactory) {
            pageFactory = new PageFactory(Props.get("automation.pages"));
        }
        return pageFactory;
    }

    public static void createWebDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        File downloadDir = new File("download");
        OsCheck checkOS = new OsCheck();
        StringBuilder pathToDriver = new StringBuilder("src/test/resources/webdrivers/");

        if (null == testInstrument) {
            testInstrument = "Firefox";
        }

        switch (BrowserEnum.valueOf(testInstrument)) {
            case Firefox:
                capabilities.setBrowserName("firefox");
                FirefoxProfile fProfile = new FirefoxProfile();
                fProfile.setAcceptUntrustedCertificates(true);
                fProfile.setPreference("browser.download.dir", downloadDir.getAbsolutePath());
                fProfile.setPreference("browser.download.folderList", 2);
                fProfile.setPreference("browser.download.manager.showWhenStarting", false);
                fProfile.setPreference("browser.helperApps.alwaysAsk.force", false);
                fProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
                fProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                        "application/zip;application/octet-stream;application/x-zip;application/x-zip-compressed");
                fProfile.setPreference("plugin.disable_full_page_plugin_for_types", "application/zip");

                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability(FirefoxDriver.PROFILE, fProfile);
                setDriver(new FirefoxDriver(capabilities));

                break;
            case Safari:
                System.setProperty("webdriver.safari.noinstall", "true");
                capabilities.setBrowserName("safari");
                setDriver(new SafariDriver(capabilities));
                break;
            case Chrome:
                HashMap<String, Object> chromePrefs = new HashMap<>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", downloadDir.getAbsolutePath());
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                switch (checkOS.getOperationSystemType()) {
                    case MacOS:
                        pathToDriver.append("mac/chromedriver");
                        break;
                    case Windows:
                        pathToDriver.append("windows/chromedriver.exe");
                        break;
                    case Linux:
                        if (checkOS.getArchType().equals("64"))
                            pathToDriver.append("linux64/chromedriver");
                        else
                            pathToDriver.append("linux32/chromedriver");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                System.setProperty("webdriver.chrome.driver", new File(pathToDriver.toString()).getAbsolutePath());
                capabilities.setBrowserName("chrome");
                setDriver(new ChromeDriver(capabilities));
                break;
            case PhantomJS:
                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability("takesScreenshot", true);

                switch (checkOS.getOperationSystemType()) {
                    case Windows:
                        pathToDriver.append("windows/phantomjs.exe");
                        break;
                    case Linux:
                        if (checkOS.getArchType().equals("64"))
                            pathToDriver.append("linux64/phantomjs");
                        else
                            pathToDriver.append("linux32/phantomjs");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                File phantomDriver = new File(pathToDriver.toString());
                capabilities.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        phantomDriver.getAbsolutePath()
                );
                System.setProperty("phantomjs.binary.path", phantomDriver.getAbsolutePath());
                setDriver(new PhantomJSDriver(capabilities));
                break;
            default:
                capabilities.setBrowserName("firefox");
                setDriver(new FirefoxDriver(capabilities));
                break;
        }
        getDriver().manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        getDriver().manage().window().setSize(new Dimension(Integer.parseInt(Props.get("browser.width")), Integer.parseInt(Props.get("browser.height"))));
    }

    public static void dispose() {
        if (os.equals("Android") || os.equals("IOS")) {
        } else {
            try {
                System.out.println("Checking any alert opened");
                WebDriverWait alertWait = new WebDriverWait(driver, 2);
                alertWait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                System.out.println("Got and alert: " + alert.getText() + "\n closing it.");
                alert.dismiss();
            } catch (Exception | Error e) {
                System.err.println("No alert opened. Closing webdriver.");
            }
            Set<String> windowsHandlerSet = Init.getDriver().getWindowHandles();
            try {
                if (windowsHandlerSet.size() > 2) {
                    windowsHandlerSet.forEach((winHandle) -> {
                        driver.switchTo().window(winHandle);
                        ((JavascriptExecutor) Init.getDriver()).executeScript(
                                "var objWin = window.self;" +
                                        "objWin.open('', '_self', '');'" +
                                        "objWin.close();");
                    });
                }

            } catch (Exception e) {
                System.err.println("Failed to kill all of the browser windows. Error message = " + e.getMessage());
            }
        }
        try {
            driver.quit();
        } catch (Exception | Error e) {
            System.err.println("Failed to quit web driver. Error message = " + e.getMessage());
        }
        setDriver(null);
        pageFactory = null;
    }

    private static WebDriver getBrowserStackDriver() {
        String browserStackUserName = "";
        String browserStackAccessKey = "";
        String url = "http://" + browserStackUserName + ":" + browserStackAccessKey + "@hub.browserstack.com/wd/hub";

        DesiredCapabilities caps = new DesiredCapabilities();
        if (os.equalsIgnoreCase("Android")) {
            caps.setCapability("browserName", "android");
            caps.setCapability("platform", "ANDROID");
        } else {
            caps.setCapability("browserName", "iPhone");
            caps.setPlatform(Platform.MAC);
        }

        caps.setCapability("device", testInstrument);
        try {
            driver = new RemoteWebDriver(new URL(url), caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return driver;
    }

    public static DriverExt getDriverExtensions() {
        return new DriverExt();
    }


    public static int getTimeOut() {
        return Integer.parseInt(timeout);
    }

    public static int getTimeOutInSeconds() {
        return Integer.parseInt(timeout) / 1000;
    }

    public static Map<String, Object> getStash() {
        return stash;
    }

    @Attachment(type = "image/png", value = "Full Screen Screenshot")
    public static byte[] takeFullScreenshot() throws AWTException, IOException {
        if (null != driver) {
            ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            java.awt.Rectangle screenBounds = new java.awt.Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage picture = new Robot().createScreenCapture(screenBounds);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ImageIO.write(picture, "png", bytes);
            return bytes.toByteArray();
        }

        return "".getBytes();
    }

    public static String getOs() {
        return os;
    }

    public static String getTestInstrument() {
        return testInstrument;
    }

    public static DatabaseConnectionFactory getDatabaseConnectionFactory() {
        if (null == dbConnectionFactory) {
            dbConnectionFactory = new DatabaseConnectionFactory();
        }
        return dbConnectionFactory;
    }
}
